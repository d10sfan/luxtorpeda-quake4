#!/bin/bash

mkdir -p ./linuxdata/

create_relative_symlink () {
        local -r target=$1
        local -r symlink="linuxdata/$target"
        mkdir -p "$(dirname "$symlink")"
        ln -rsf "$target" "$symlink"
}

if [ ! -f ready ]; then
	find {q4base,q4mp} -type f  | while read -r file_name ; do
        	create_relative_symlink "$file_name"
	done

	cp -r linuxofficial/* linuxdata # Copying instead of symlink, because the engine can't find the original data (it changes the path to linuxofficial incorrectly)

	touch ready
fi

cd linuxdata
LD_LIBRARY_PATH=. ./quake4smp.x86
