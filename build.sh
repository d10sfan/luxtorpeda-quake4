#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

cp -rfv linuxofficial "2210/dist/"
cp -rfv run-quake4.sh "2210/dist/run-quake4.sh"
